# Player on Game
extends KinematicBody2D

const ACELERACION = 500
const MAX_SPEED = 150
const FRICCION = 500

var ideas = 0
var velocidad = Vector2.ZERO
var animationPlayer = null
var direction = "right"

var path := PoolVector2Array() setget set_path

func set_path(value: PoolVector2Array) -> void:
	path = value
	if value.size() == 0:
		return
	set_process(true)
	
func _process(delta: float) -> void:
	var move_distance := MAX_SPEED * delta
	move_along_path(move_distance)

func move_along_path(distance: float) -> void:
	var start := position
	if path.size() == 0:
		if direction == "right":
			# Animaciones previamente hechas
			print("aca toy")
			animationPlayer.play("IdleRight")
		else:
			animationPlayer.play("IdleLeft")
		return
	for i in range(path.size()):
		var next_distance := start.distance_to(path[0])
		if distance <= next_distance and distance >= 0.0:			
			var move_vector = path[0] - start
			print(move_vector)
			position = start.linear_interpolate(path[0], distance / next_distance)
			if move_vector.x > 0:
				# Animaciones previamente hechas
				animationPlayer.play("RunRight")
				direction = "right"
				print("aqui toy")
			else:
				direction = "left"
				animationPlayer.play("RunLeft")
			break
		elif distance < 1.0:
			position = path[0]
			set_process(false)
		distance -= next_distance
		start = path[0]
		path.remove(0)
			
#onready var player =  $AnimationPlayer
#onready var animationTree = $AnimationTree
#onready var animationState = animationTree.get("parameters/playback")
func cart2iso(coord):
	return Vector2(coord.x - coord.y, (coord.x + coord.y) / 2)

func iso2cart(coord):
	return Vector2((coord.x + coord.y * 2) / 2, (coord.y * 2 - coord.x) / 2)


func incrementar_ideas():
	ideas += 1	
	$IdeaSound.play()
	for node in GameManager.UI.get_children():		
		if node is Label:
			node.set_text(String(ideas))

func obtener_ideas():
	return ideas
#
#func _physics_process(delta):
#	var vector_entrada = Vector2.ZERO
#	vector_entrada.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
#	vector_entrada.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
#	vector_entrada = vector_entrada.normalized()
#
#
#	if vector_entrada != Vector2.ZERO:
#		if vector_entrada.x > 0:
#			# Animaciones previamente hechas
#			animationPlayer.play("RunRight")
#			direction = "right"
#		else:
#			direction = "left"
#			animationPlayer.play("RunLeft")
#		velocidad = velocidad.move_toward(vector_entrada * MAX_SPEED, ACELERACION * delta)		
#	else:
#		velocidad = velocidad.move_toward(Vector2.ZERO, FRICCION * delta)
#		if direction == "right":
#			# Animaciones previamente hechas
#			animationPlayer.play("IdleRight")
#		else:
#			animationPlayer.play("IdleLeft")
#	velocidad = move_and_slide(velocidad)

func _ready():
	GameManager.Player = self
	animationPlayer = $AnimationPlayer

