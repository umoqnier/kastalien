extends Node2D

onready var nav: Navigation2D = $Navigation2D
onready var line: Line2D = $Line2D
onready var character: KinematicBody2D = $Player

func _unhandled_input(event: InputEvent) -> void:
	if not event is InputEventMouseButton:
		return
	if event.button_index != BUTTON_LEFT or not event.pressed:
		return
	var start := character.global_position
	var goal = event.global_position
	var path := nav.get_simple_path(start, goal, false)
	line.points = path

	character.path = path
