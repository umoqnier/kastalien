tool
extends TileMap

func _ready():
	var offset = Vector2(-18, -129)

	var polygon = NavigationPolygon.new()
	var vertices = PoolVector2Array([Vector2(82, 129), Vector2(18, 161), Vector2(82, 193), Vector2(146, 161)])
	polygon.set_vertices(vertices)
	var indices = PoolIntArray([0, 1, 2, 3])
	polygon.add_polygon(indices)
	
	for tile in tile_set.get_tiles_ids().size():
		tile_set.tile_set_texture_offset(tile, offset)
		tile_set.tile_set_navigation_polygon_offset(tile, offset)
		if tile >= 61 and tile <= 64:
			tile_set.tile_set_navigation_polygon(tile, polygon)
		if tile >= 93 and tile <= 96:
			tile_set.tile_set_navigation_polygon(tile, polygon)
		#else:
		#	tile_set.tile_set_navigation_polygon(tile, null)
