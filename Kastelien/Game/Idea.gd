extends Area2D

var estrategias = ["Aplicar un programa\nsustentable de\n Reforestación",
"Aplicar el sistema de\nagroforesta para\n conseguir alimentos",
"Plantar cañamo"]
var idea_actual

func _on_Area2D_body_entered(body):
	GameManager.Player.incrementar_ideas()
	idea_actual = GameManager.Player.obtener_ideas()
	print(idea_actual)
	GameManager.Summary.Label.text = estrategias[idea_actual - 1]
	queue_free()
